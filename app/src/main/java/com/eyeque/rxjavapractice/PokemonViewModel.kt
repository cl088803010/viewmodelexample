package com.eyeque.rxjavapractice

import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.eyeque.rxjavapractice.room.FavPokemon
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers
import java.lang.IllegalArgumentException


class PokemonViewModel(val repository: Repository): ViewModel() {

    private val _pokemonList: MutableLiveData<List<Pokemon>> = MutableLiveData()
    val pokemonList: LiveData<List<Pokemon>> = _pokemonList

    private val _favPokemonList: MutableLiveData<List<FavPokemon>> = MutableLiveData()
    val favPokemonList: LiveData<List<FavPokemon>> = _favPokemonList

    fun getPokemons(){
        repository.getPokemonResponse()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    val pokemonList = ArrayList<Pokemon>()
                    it.results.forEach {
                        val url = it.url
                        val subUrlArray = url.split("/")
                        val urlSb = StringBuilder()
                        urlSb.append(IMAGE_BASE_URL).append(subUrlArray[subUrlArray.size - 2]).append(".png")
                        Log.i("PokemonViewModel","IMAGE URL IS: $urlSb")
                        it.url = urlSb.toString()
                        pokemonList.add(it)
                    }
                    _pokemonList.value = pokemonList
                },
                {
                    it.printStackTrace()
                }
            )
    }

    fun insertFavPokemon(favPokemon: FavPokemon){
        Observable.fromCallable {
            repository.insertFavPokemon(favPokemon)
        }.subscribeOn(Schedulers.io()).subscribe(
            {
                Log.i("PokemonVm","insert done")
            },
            {
                it.printStackTrace()
            }
        )
    }

    fun getFavPokemons(){
        repository.favPokemons().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(
            {
                _favPokemonList.value = it
            },
            {
                it.printStackTrace()
            }
        )
    }

    fun deleteAll(){
        Observable.fromCallable {
            repository.deleteAll()
        }.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(
            {
                Log.i("PokemonVm","delete done")
            },
            {
                it.printStackTrace()
            }
        )
    }

    companion object{
        const val IMAGE_BASE_URL = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/"
    }

}

class PokemonViewModelFactory(val repository: Repository): ViewModelProvider.Factory{
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(PokemonViewModel::class.java)){
            return PokemonViewModel(repository) as T
        }
        throw IllegalArgumentException("unknown viewmodel class")
    }

}