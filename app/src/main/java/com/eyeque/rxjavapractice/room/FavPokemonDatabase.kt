package com.eyeque.rxjavapractice.room
import android.content.Context
import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.Room

@Database(entities = [FavPokemon::class], version = 1, exportSchema = false)
abstract class FavPokemonDatabase: RoomDatabase(){

    abstract fun favPokemonDao(): FavPokemonDao

    companion object{
        @Volatile
        var mInstance: FavPokemonDatabase? = null

        fun getInstance(context: Context): FavPokemonDatabase{
            return mInstance ?: synchronized(this){
                val instance = Room
                    .databaseBuilder(
                        context.applicationContext,
                        FavPokemonDatabase::class.java,
                        "fav_pokemon_database")
                    .build()
                mInstance = instance
                instance
            }
        }
    }
}

