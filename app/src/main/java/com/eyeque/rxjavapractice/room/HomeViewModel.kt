package com.eyeque.rxjavapractice.room

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.eyeque.rxjavapractice.Pokemon
import com.eyeque.rxjavapractice.Repository
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers

class HomeViewModel(val repository: Repository): ViewModel() {

    private val _pokemonList: MutableLiveData<List<Pokemon>> = MutableLiveData()
    val pokemonList: LiveData<List<Pokemon>> = _pokemonList

    fun getPokemonList(){
        repository.getPokemonResponse()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    val pokemonList = ArrayList<Pokemon>()
                    it.results.forEach {
                        val url = it.url
                        val subUrlArray = url.split("/")
                        val urlSb = StringBuilder()
                        urlSb.append(IMAGE_BASE_URL).append(subUrlArray.get(subUrlArray.size - 2)).append(".png")
                        Log.i("HomeViewModel", urlSb.toString())
                        it.url = urlSb.toString()
                        pokemonList.add(it)
                    }
                    _pokemonList.value = pokemonList
                },
                {
                    it.printStackTrace()
                }
            )
    }

    companion object{
        const val IMAGE_BASE_URL = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/"
    }
}

class HomeViewModelFarctory(val repository: Repository) : ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(HomeViewModel::class.java)){
            return HomeViewModel(repository) as T
        }
        throw IllegalArgumentException("unknow view model")
    }

}