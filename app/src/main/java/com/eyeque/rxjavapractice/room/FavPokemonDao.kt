package com.eyeque.rxjavapractice.room

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import io.reactivex.rxjava3.core.Flowable

@Dao
interface FavPokemonDao {

    @Insert(onConflict = REPLACE)
    fun insertFavPokemon(favPokemon: FavPokemon)

    @Query("DELETE FROM fav_pokemon_table")
    fun deleteAll()

    @Query("SELECT * FROM fav_pokemon_table")
    fun favPokemons(): Flowable<List<FavPokemon>>

    @Query("DELETE FROM fav_pokemon_table WHERE name = :pokemonName")
    fun deletePokemon(pokemonName: String)
}