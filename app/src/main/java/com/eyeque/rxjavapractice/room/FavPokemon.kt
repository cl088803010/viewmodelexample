package com.eyeque.rxjavapractice.room

import androidx.room.Entity
import androidx.room.ColumnInfo
import androidx.room.PrimaryKey

@Entity(tableName = "fav_pokemon_table")
data class FavPokemon(
    @ColumnInfo val name: String,
    @ColumnInfo val url: String,
    @PrimaryKey(autoGenerate = true) val id: Int = 0)