package com.eyeque.rxjavapractice

import io.reactivex.rxjava3.core.Observable
import retrofit2.http.GET
import retrofit2.http.Header

interface API {
    @GET("index.php/rest/V1/eyecloud/api/V2/getdashboardsummary")
    fun getDashboardSummary(@Header("Authorization") token: String): Observable<DashboardSummary>

    @GET("/api/v2/pokemon")
    fun getPokemonResponse(): Observable<PokemonResponse>
}