package com.eyeque.rxjavapractice

import io.reactivex.rxjava3.core.Observable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class APIClient {
    val retrofit: Retrofit
    val api: API

    init {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        val okHttpClient = OkHttpClient().newBuilder().addInterceptor(httpLoggingInterceptor).build()
        retrofit = Retrofit.Builder().baseUrl(BASE_URL).client(okHttpClient).addConverterFactory(GsonConverterFactory.create()).addCallAdapterFactory(RxJava3CallAdapterFactory.create()).build()
        api = retrofit.create(API::class.java)
    }


    companion object{
        const val TOKEN = "1hny1fwpku0wdbxs3lzph7312bihuxzg"
        const val BASE_URL = "https://pokeapi.co"

        var mInstance: APIClient? = null

        fun getInstance(): APIClient = mInstance ?: APIClient()
    }
}