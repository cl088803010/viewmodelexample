package com.eyeque.rxjavapractice

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.card.MaterialCardView

class PokemonsRecyclerViewAdapter(val pokemonList: ArrayList<Pokemon>): RecyclerView.Adapter<PokemonsRecyclerViewAdapter.ViewHolder>() {

    var onPokemonCardClicked: ((Pokemon) -> Unit)? = null
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val pokemonTv: TextView
        val pokemonIv: ImageView
        val pokemonCv: MaterialCardView
        init {
            pokemonTv = itemView.findViewById(R.id.pokemonTv)
            pokemonIv = itemView.findViewById(R.id.pokemonIv)
            pokemonCv = itemView.findViewById(R.id.pokemonCv)
        }
        fun bind(pokeMon: Pokemon){
            Glide.with(itemView.context).load(pokeMon.url).into(pokemonIv)
            pokemonTv.setText(pokeMon.name)

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.pokemon_item_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(pokemonList.get(position))
        holder.pokemonCv.setOnClickListener {
            onPokemonCardClicked?.invoke(pokemonList.get(position))
        }
    }

    override fun getItemCount(): Int {
        return pokemonList.size
    }

    fun addPokemons(pokemons: List<Pokemon>){
        pokemonList.addAll(pokemons)
        this.notifyItemInserted(0)
    }
}