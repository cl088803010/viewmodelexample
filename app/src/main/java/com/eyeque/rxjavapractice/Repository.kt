package com.eyeque.rxjavapractice

import com.eyeque.rxjavapractice.room.FavPokemon
import com.eyeque.rxjavapractice.room.FavPokemonDao
import io.reactivex.rxjava3.core.Observable

class Repository(val favPokemonDao: FavPokemonDao,val api: API) {

    fun getPokemonResponse(): Observable<PokemonResponse>{
        return api.getPokemonResponse()
    }

    fun insertFavPokemon(favPokemon: FavPokemon){
        favPokemonDao.insertFavPokemon(favPokemon)
    }

    fun favPokemons() = favPokemonDao.favPokemons()

    fun deleteAll() = favPokemonDao.deleteAll()
}