package com.eyeque.rxjavapractice

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import android.widget.Button
import androidx.activity.viewModels
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.adapter.StatefulAdapter
import androidx.viewpager2.widget.ViewPager2
import com.eyeque.rxjavapractice.fragment.ContainerFragment
import com.eyeque.rxjavapractice.fragment.FavFragment
import com.eyeque.rxjavapractice.fragment.HomeFragment
import com.eyeque.rxjavapractice.room.FavPokemon
import com.eyeque.rxjavapractice.room.FavPokemonDatabase

class PokemonActivity : AppCompatActivity() {
     val vm: PokemonViewModel by viewModels {
         PokemonViewModelFactory(Repository(FavPokemonDatabase.getInstance(applicationContext).favPokemonDao(), APIClient.getInstance().api))
     }
    val adapter: PokemonsRecyclerViewAdapter = PokemonsRecyclerViewAdapter(arrayListOf())
    lateinit var pokemonRv: RecyclerView
    lateinit var viewPager: ViewPager2
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //initRv()
        //setupDataObserver()
        //vm.getPokemons()
        setupContainerFragment()
    }

    private fun setupDataObserver(){
        vm.pokemonList.observe(this){
            adapter.addPokemons(it)
        }

        vm.favPokemonList.observe(this){
            it.forEach {Log.i("PokemonActivity", it.url) }
        }
    }

    private fun setBtn(){
        val btn = findViewById<Button>(R.id.getBtn)
        btn.setOnClickListener {
            vm.deleteAll()
        }
    }

    private fun initRv(){
        pokemonRv = findViewById(R.id.pokemonRv)
        pokemonRv.adapter = adapter
        pokemonRv.layoutManager = LinearLayoutManager(applicationContext, LinearLayoutManager.VERTICAL, false)
    }

    private fun setupContainerFragment() {
        supportFragmentManager.beginTransaction().replace(R.id.container, ContainerFragment(), "ContainerFragment").commit()
    }
}