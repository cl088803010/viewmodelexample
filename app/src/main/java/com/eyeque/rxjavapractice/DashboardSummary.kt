package com.eyeque.rxjavapractice

import com.google.gson.annotations.SerializedName

class DashboardSummary {
    /**
     * sph_od : -5.75
     * sph_os : -3.5
     * cyl_od : -0.5
     * cyl_os : -1.75
     * axis_od : 86
     * axis_os : 139
     * pd : 63
     * nvadd : 0
     * no_egns : 18
     * no_tests : 19
     * accuracy_gap_od : 0
     * accuracy_gap_os : 0
     * grade_od : Low
     * grade_os : Low
     * first_test_at : 2020-10-01T20:38:28Z
     * updated_at : 2020-10-05T22:06:21Z
     * piechart_url : http://34.216.211.131/userdata/users/piechart.html?hash=5bc7da0ab5ada73ad531335ea72bd6c863d46483
     * no_vc_tests : 38
     * valid_error_radius_od : false
     * valid_error_radius_os : false
     * no_discarded_tests : 23
     * no_removed_tests_od : 4
     * no_removed_tests_os : 5
     * no_valid_tests_od : 15
     * no_valid_tests_os : 14
     */
    @SerializedName("sph_od")
    var sphOd = 0.0

    @SerializedName("sph_os")
    var sphOs = 0.0

    @SerializedName("cyl_od")
    var cylOd = 0.0

    @SerializedName("cyl_os")
    var cylOs = 0.0

    @SerializedName("axis_od")
    var axisOd = 0

    @SerializedName("axis_os")
    var axisOs = 0
    var pd = 0.0
    var nvadd: String? = null

    @SerializedName("no_egns")
    var numEgns = 0

    @SerializedName("no_tests")
    var numTests = 0

    @SerializedName("accuracy_gap_od")
    var accuracyGapOd = 0

    @SerializedName("accuracy_gap_os")
    var accuracyGapOs = 0

    @SerializedName("grade_od")
    var gradeOd: String? = null

    @SerializedName("grade_os")
    var gradeOs: String? = null

    @SerializedName("first_test_at")
    var firstTestAt: String? = null

    @SerializedName("updated_at")
    var updatedAt: String? = null

    @SerializedName("piechart_url")
    var piechartUrl: String? = null

    @SerializedName("no_vc_tests")
    var numVcTests = 0

    @SerializedName("valid_error_radius_od")
    var isValidErrorRadiusOd = false

    @SerializedName("valid_error_radius_os")
    var isValidErrorRadiusOs = false

    @SerializedName("no_discarded_tests")
    var numDiscardedTests = 0

    @SerializedName("no_removed_tests_od")
    var numRemovedTestsOd = 0

    @SerializedName("no_removed_tests_os")
    var numRemovedTestsOs = 0

    @SerializedName("no_valid_tests_od")
    var numValidTestsOd = 0

    @SerializedName("no_valid_tests_os")
    var numValidTestsOs = 0
}