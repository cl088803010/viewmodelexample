package com.eyeque.rxjavapractice.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eyeque.rxjavapractice.Pokemon
import com.eyeque.rxjavapractice.R
import com.eyeque.rxjavapractice.room.FavPokemon
import com.google.android.material.card.MaterialCardView

class FavPokemonRecyclerViewAdapter(val favPokemonList: ArrayList<FavPokemon>) :
    RecyclerView.Adapter<FavPokemonRecyclerViewAdapter.ViewHolder>() {

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val pokemonTv: TextView
        val pokemonIv: ImageView
        val pokemonCv: MaterialCardView
        init {
            pokemonTv = itemView.findViewById(R.id.pokemonTv)
            pokemonIv = itemView.findViewById(R.id.pokemonIv)
            pokemonCv = itemView.findViewById(R.id.pokemonCv)
        }
        fun bind(favPokemon: FavPokemon){
            Glide.with(itemView.context).load(favPokemon.url).into(pokemonIv)
            pokemonTv.setText(favPokemon.name)

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.pokemon_item_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(favPokemonList.get(position))
    }

    override fun getItemCount(): Int {
        return favPokemonList.size
    }

    fun addPokemons(favPokemons: List<FavPokemon>){

//        val size = favPokemonList.size
//        favPokemonList.clear()
//        notifyItemRangeRemoved(0, size)

        favPokemonList.addAll(favPokemons)
        this.notifyItemInserted(0)
    }
}