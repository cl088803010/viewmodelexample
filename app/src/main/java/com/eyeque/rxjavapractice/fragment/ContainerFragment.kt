package com.eyeque.rxjavapractice.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.eyeque.rxjavapractice.R
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class ContainerFragment : Fragment() {

    private lateinit var viewPager: ViewPager2
    private lateinit var tabLayout: TabLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_container, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewPager = view.findViewById(R.id.pager)
        tabLayout = view.findViewById(R.id.tab_layout)
        setupViewPager()
        setupTab()
    }

    private fun setupTab(){
        TabLayoutMediator(tabLayout, viewPager){
            tab, position ->
                if(position == 0){
                    tab.text = "Home"
                }else{
                    tab.text = "Fav"
                }
        }.attach()
    }

    private fun setupViewPager(){
        viewPager.adapter = ViewPagerAdapter(this)
    }

    class ViewPagerAdapter(fragment: Fragment): FragmentStateAdapter(fragment){
        override fun getItemCount(): Int {
            return 2
        }

        override fun createFragment(position: Int): Fragment {
            if (position == 0){
                return HomeFragment()
            }else{
                return FavFragment()
            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = ContainerFragment()

    }
}