package com.eyeque.rxjavapractice.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eyeque.rxjavapractice.*
import com.eyeque.rxjavapractice.adapter.FavPokemonRecyclerViewAdapter
import com.eyeque.rxjavapractice.room.FavPokemon
import com.eyeque.rxjavapractice.room.FavPokemonDatabase

class FavFragment : Fragment() {

    private lateinit var favRv: RecyclerView
    private val adapter = FavPokemonRecyclerViewAdapter(arrayListOf())
    private val pokemonViewModel: PokemonViewModel by viewModels {
        PokemonViewModelFactory(Repository(FavPokemonDatabase.getInstance(requireContext().applicationContext).favPokemonDao(), APIClient.getInstance().api))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fav, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        favRv = view.findViewById(R.id.favRv)
        setupRv()
        pokemonViewModel.getFavPokemons()
        setupObeserve()
    }

    override fun onResume() {
        super.onResume()
    }

    private fun setupRv(){

        favRv.adapter = adapter
        favRv.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
    }

    private fun setupObeserve(){
        pokemonViewModel.favPokemonList.observe(viewLifecycleOwner){
            adapter.addPokemons(it)
        }
    }

    companion object {

        @JvmStatic
        fun newInstance() = FavFragment()

    }
}