package com.eyeque.rxjavapractice.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eyeque.rxjavapractice.*
import com.eyeque.rxjavapractice.room.FavPokemon
import com.eyeque.rxjavapractice.room.FavPokemonDatabase
import com.eyeque.rxjavapractice.room.HomeViewModel
import com.eyeque.rxjavapractice.room.HomeViewModelFarctory

class HomeFragment : Fragment() {

    private lateinit var homeRv: RecyclerView
    private val adapter = PokemonsRecyclerViewAdapter(arrayListOf())
    private val pokemonViewModel: PokemonViewModel by viewModels {
        PokemonViewModelFactory(Repository(FavPokemonDatabase.getInstance(requireContext().applicationContext).favPokemonDao(), APIClient.getInstance().api))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        homeRv = view.findViewById(R.id.homeRv)
        setupRv()
        pokemonViewModel.getPokemons()
        setupObeserve()
    }

    private fun setupRv(){
        adapter.onPokemonCardClicked = {
            val favPokemon = FavPokemon(it.name, it.url)
            pokemonViewModel.insertFavPokemon(favPokemon)
        }
        homeRv.adapter = adapter
        homeRv.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
    }

    private fun setupObeserve(){
        pokemonViewModel.pokemonList.observe(viewLifecycleOwner){
            adapter.addPokemons(it)
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = HomeFragment()
    }
}